# gitbook-reduce

Reduce Gitbook to one page HTML

## Installation

```
opam pin add --yes https://github.com/minimalprocedure/gitbook-reduce.git
opam install gitbook-reduce
```
