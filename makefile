.PHONY: all
all: 
	dune build

clean:
	rm ./_build -Rv

#.PHONY: dist
dist:
	dune build
	dune install --prefix ./_dist