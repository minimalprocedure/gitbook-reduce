open Soup

let docpath base name = Filename.concat base name

let opendoc source = source |> read_file |> parse

let body_inner doc = doc $ "div[class~=\"body-inner\"]"

let chapters summary = summary $$ "> li[class~=\"chapter\"]"

let chapter_anchor chapter = chapter $ "> a"

let chapter_anchor_href chapter = chapter |> R.attribute "href"

let idize href = href |> Str.global_replace (Str.regexp "[\\.|\\/]") "_"

let remove = function
  | None -> ()
  | Some(n) -> delete n

let deanchorize = function
  | None -> ()
  | Some(h1) -> 
    let a = (h1 $ "a") in 
    let text = a |> R.leaf_text in
    replace a (create_text text)

let articles chapter = chapter $? "> ul[class~=\"articles\"]"

let clear_body body = 
  (body $? "div[class~=\"search-results\"]") |> remove;
  (body $? "div[class~=\"book-header\"] > h1 > i") |> remove;
  (body $? "div[class~=\"book-header\"] > h1") |> deanchorize

let rec process_chapter root chapters_list chapter = 
  let anchor = (chapter |> chapter_anchor) in  
  let body_inner = 
    (docpath root (anchor |> chapter_anchor_href)) |> opendoc 
    |> body_inner 
  in
  let id = anchor |> R.attribute "href" |> idize in
  anchor |> set_attribute "href" ("#" ^ id);
  body_inner |> set_attribute "id" id;
  body_inner |> clear_body;

  "processing: " ^ (chapter |> chapter_anchor |> chapter_anchor_href) |> print_endline;

  let chapters_list = chapters_list @ [body_inner] in
  match chapter |> articles with
  | None -> chapters_list
  | Some(articles) -> 
    (articles |> chapters) 
    |> fold (fun chapters_list chapter -> 
        chapter |> process_chapter root chapters_list
      ) chapters_list 

let save_page path source = 
  FileUtil.mkdir path;
  write_file (docpath path "index.html") source

let set_title head = function
  | Some(title) -> replace (head $ "title") (create_element "title" ~inner_text:title)
  | _ -> ()

let render_page ?title tplpath nodes =
  let template = (docpath tplpath "index.html") |> opendoc in
  let head = template $ "head" in
  (create_element "style" ~inner_text:((docpath tplpath "style.css") |> read_file)) 
  |> append_child head;

  title |> set_title head;

  let summary = template $ "div[class~=\"book-summary\"]" in 
  let book = template $ "div[class~=\"book-body\"]" in
  match nodes with
  | [] -> ""
  | hd :: tl -> 
    hd |> append_child summary;
    tl |> List.iter (fun c -> c |> append_child book);
    template |> pretty_print

let process_index root index = 
  let index = (docpath root index) |> opendoc in
  let summary = index $ "div[class~=\"book-summary\"] ul[class~=\"summary\"]" in
  summary |> chapters |> fold (
    fun chapters_list chapter -> chapter |> process_chapter root chapters_list
  ) [summary]
