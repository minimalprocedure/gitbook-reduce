open Cmdliner

let template_path () = 
  let basename = Sys.executable_name |> Filename.basename in
  let dirname = Sys.executable_name |> Filename.dirname |> Filename.dirname in
  [dirname; "share"; basename; "template"] |> List.fold_left (fun acc p -> Filename.concat acc  p) ""

let process root output template title =
  let tplfolder = match template with 
    | None -> template_path ()
    | Some(template) -> template
  in
  Gitbook_reduce.(
    process_index root "index.html" 
    |> render_page tplfolder ?title 
    |> save_page output
  )

let progname = "gitbook-reduce"

let index = 
  let doc = "Gitbook index file" in
  Arg.(
    required & pos 
      ~rev:true 1 (some string) None & info [] 
      ~docv:"SOURCE_INDEX"
      ~doc
  )

let output = 
  let doc = "Output folder" in
  Arg.(
    required & pos 
      ~rev:true 0 (some string) None & info [] 
      ~docv:"DESTINATION_FOLDER"
      ~doc
  )

let template =
  let doc = "Template folder." in
  Arg.(value & opt (some string) None & info ["t"; "T"; "template"] ~doc)

let title =
  let doc = "Set HTML title." in
  Arg.(value & opt (some string) None & info ["h"; "H"; "heading"] ~doc)

let cmd = 
  let doc = "Reduce Gitbook to one page HTML" in
  let exits = Term.default_exits in
  let man = [] in
  Term.(const process $ index $ output $ template $ title),
  Term.info progname ~version:"v0.4.0" ~doc ~exits ~man

let () = Term.exit (Term.eval cmd)
